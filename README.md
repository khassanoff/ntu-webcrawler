Author:Benjamin Bigot  
Additional Modifications by: Phuah Chee Chong, Rakhi Singh  
Contact: benjbigot@gmail.com  
Creation: 2013  
Last changes: 2014  


webNews_Crawler  
===============  

News Website crawler using Python 3.4.

Before installation please ensure the following has been installed  
1.	Python 3.4.x  
2.	ActivePerl 5.16  

After the above installation, please follow the instructions for your respective OS.  

To run the program, launch initialise.bat or initialise.sh (Windows or Ubuntu respectively)  


Installation Notes (Tested Windows 7 and above)  
==================  

Steps to install Program  
  
1.	Open CMD and navigate to python root folder. Run the following: python -m pip install urllib3   
2.	Install lxml from http://www.lfd.uci.edu/~gohlke/pythonlibs/  
3.	Open CMD and navigate to python root folder. Run the following: python -m pip install python-dateutil

Install Perl Modules  
Please ensure to only install ActivePerl 5.16  
  
1.	Type the following in the CMD: ppm install XML-LibXML (Case Sensitive)  
2.	Type the following in the CMD: ppm install Lingua-EN-Numbers (Case Sensitive)  
3.	Download and replace the file Numbers.pm from the "scripts" Folder

Installation Notes (Tested on Ubuntu LTS 14.04)  
==================  
Make sure the default python installation is updated to point to the latest python version  
You can do so in the terminal with "alias python=python3.4"  
Otherwise please update the sh file such that it uses python3.4  
  
Steps to install Program  
Open Terminal and enter the following commands  
  
1.	Install lxml - sudo pip3.4 install lxml (Make sure pip version is correct!)  
2.	Install urllib3 - sudo pip3.4 install urllib3   
3.	Install dateutil - sudo pip3.4 install python-dateutil  
  
Perl  
  
1.	cpan install XML::LibXML  
2.	cpan install Lingua::EN::Numbers  
3.	Download and replace the file Numbers.pm from the "scripts" Folder